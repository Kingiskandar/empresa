from django.db import models
from django.contrib.auth.models import User

class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    department = models.CharField(max_length=100)

class Profile (models.Model):
    fecha_nacimiento=models.DateField()
    imagen-perfil=models.ImageField(upload_to='empresa')

class Cliente (models.Model):
    id_cliente=models.BigAutoField(primary_key=True)
    apellido=models.CharField(max_length=30)
    nombre=models.CharField(max_length=30)
    fecha_alta=models.DateField()
    porcentaje_descuento=models.PositiveIntegerField()
    def __str__(self):
        texto='{} {}'.format(
            self.nombre,
            self.apellido,
            self.fecha_alta,
            self.porcentaje_descuento)
        return texto


class Producto (models.Model):
    id_producto=models.BigAutoField(primary_key=True)
    nombre_producto=models.CharField(max_length=30)
    precio_producto=models.PositiveIntegerField()
    cantidad_existente=models.PositiveIntegerField()

class Factura (models.Model):
    id_factura=models.BigAutoField(primary_key=True)
    nro_factura=models.PositiveIntegerField()
    fecha_factura=models.DateField()
    condicion_venta=models.CharField(max_length=30)
    id_cliente=models.ForeignKey(Cliente, on_delete=models.CASCADE)

class DetalleFactura (models.Model):
    id_factura=models.ForeignKey(Factura, on_delete=models.CASCADE)
    id_detalle_factura=models.BigAutoField(primary_key=True)
    id_producto=models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad_vendida=models.PositiveIntegerField()
    precio_venta=models.PositiveIntegerField()

# Create your models here.
