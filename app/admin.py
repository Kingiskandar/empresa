from django.contrib import admin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User
admin.site.register(User, UserAdmin)

from .models import Cliente
admin.site.register (Cliente)
#no agregar mas de un registro

from .models import Producto
admin.site.register (Producto)

from .models import Factura
admin.site.register (Factura)

from .models import DetalleFactura
admin.site.register (DetalleFactura)
# Register your models here.
