FROM python
    RUN mkdir /empresa
    
    WORKDIR /empresa
    
    ADD . /empresa/

    RUN pip install -r requerimientos.txt
